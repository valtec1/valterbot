package Solutis_Valter;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * ValterBot - a robot by Valter Gomes Neto
 */
public class ValterBot extends Robot
{
	/**
	 * run: ValterBot's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setColors(Color.blue,Color.yellow,Color.red); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			ahead(100);
			turnGunRight(360);
			back(100);
			turnGunRight(360);
		}
	}
	
	//todos metodos abaixo estao passando como referência em pixel

	/**
	 * onScannedRobot: What to do when you see another robot / o que ele faz quando encontra outro robo no radar
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like 
		
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet / o que ele faz quando é atingido
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall / se o seu robo bater na parede
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	
}
